import React from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';
import { getAccessToken } from './accessTokenUtils';

const PrivateRoute: React.FC<RouteProps> = ({
  component: Component,
  ...rest
}: RouteProps) => {
  const condition = getAccessToken('accessToken') !== '';
  return condition ? (
    <Route {...rest} component={Component} />
  ) : (
    <Redirect to="/home" />
  );
};
export default PrivateRoute;
