export const setAccessToken: (
  name: string,
  value: string
) => void = (name, value) => {
  localStorage.setItem(name, value);
};

export const getAccessToken: (name: string) => string = (name) => {
  return localStorage.getItem(name) ?? '';
};

export const eraseAccessToken: (name: string) => void = (name) => {
  localStorage.removeItem(name);
};
