import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { getAccessToken } from './accessTokenUtils';

const ProtectedRoute: React.FC<{
  component: React.FC;
  path: string;
  exact: boolean;
}> = (props) => {
  const condition = getAccessToken('accessToken') === '';

  return condition ? <Route {...props} /> : <Redirect to="/" />;
};
export default ProtectedRoute;
