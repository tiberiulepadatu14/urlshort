import { eraseAccessToken, getAccessToken } from './accessTokenUtils';
import { IURL, IUser, IUserData, IUserRegister } from './interface';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const axios = require('axios');

export { eraseAccessToken };
const SRV_ROOT = "http://localhost:3030/";

export const register: (state: IUserRegister) => unknown = async (state) => {
  const data = JSON.stringify({
    email: state.userName,
    password: state.password,
  });

  const config = {
    method: 'post',
    url: 'http://localhost:3030/users',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    data: data,
  };

  return await axios(config);
};

export const logIn: (state: IUser) => unknown = async (state) => {
  const data = JSON.stringify({
    strategy: 'local',
    email: state.userName,
    password: state.password,
  });

  const config = {
    method: 'post',
    url: '/authentication',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    data: data,
  };
  return await axios(config);
};

const configUrlCRUD = () => {
  return {
    url: `${SRV_ROOT}crud-url`,
    headers: {
      Authorization: getAccessToken('accessToken'),
      'Content-Type': 'application/json',
    },
  };
};

export const createUrl: (state: { url: string }) => unknown = async (state) => {
  return await axios({
    ...configUrlCRUD(),
    method: 'post',
    data: state,
  });
};

interface IRequest {
  lastPage?: boolean;
  page?: number;
  urls?: { data: { data: Array<IURL> } };
}

export const readUrls: (
  page?: number,
  limit?: number
) => Promise<IRequest> = async (page, limit = 6) => {
  page = (page ?? 0) < 0 ? 0 : page;
  const urls = await axios({
    ...configUrlCRUD(),
    url: `${configUrlCRUD().url}?$limit=${limit}${page ? `&$skip=${page * limit}` : ''
      }`,
    method: 'get',
  });

  if (page && urls.data.total !== 0 && urls.data.data.length === 0) {
    return {
      lastPage: true,
      page: Math.ceil((urls.data.total - limit) / limit),
      urls: await axios({
        ...configUrlCRUD(),
        url: `${configUrlCRUD().url}?$limit=${limit}${page ? `&$skip=${urls.data.total - limit}` : ''
          }`,
        method: 'get',
      }),
    };
  }

  return {
    lastPage: ((page ?? 0) + 1) * limit >= urls.data.total,
    page,
    urls,
  };
};

export const updateUrl = async (urlsID: string, data: IURL) => {
  return await axios({
    ...configUrlCRUD(),
    url: `${configUrlCRUD().url}/${urlsID}`,
    method: 'put',
    data: JSON.stringify(data),
  });
};

export const deleteURL = async (urlsID: string) => {
  return await axios({
    ...configUrlCRUD(),
    url: `${configUrlCRUD().url}/${urlsID}`,
    method: 'delete',
  });
};

// Create user is done with the register api

export const readUsers = async (page?: number, limit: number = 6) => {
  const config = {
    method: 'get',
    url: `http://localhost:3030/users?$limit=${limit}${page ? `&$skip=${page * limit}` : ''
      }`,
    headers: {
      Authorization: getAccessToken('accessToken'),
      'Content-Type': 'application/json',
    },
  };

  page = (page ?? 0) < 0 ? 0 : page;
  const users = await axios({
    ...config,
  });

  if (page && users.data.total !== 0 && users.data.data.length === 0) {
    return {
      lastPage: true,
      page: Math.ceil((users.data.total - limit) / limit),
      users: await axios({
        ...configUrlCRUD(),
        url: `${configUrlCRUD().url}?$limit=${limit}${page ? `&$skip=${users.data.total - limit}` : ''
          }`,
        method: 'get',
      }),
    };
  }

  return {
    lastPage: ((page ?? 0) + 1) * limit >= users.data.total,
    page,
    users,
  };
};

export const updateUser = async (userID: string, data: IUserData) => {
  return await axios({
    headers: {
      Authorization: getAccessToken('accessToken'),
      'Content-Type': 'application/json',
    },
    url: `http://localhost:3030/users/${userID}`,
    method: 'patch',
    data: JSON.stringify(data),
  });
};

export const deleteUser = async (userID: string) => {
  return await axios({
    method: 'delete',
    headers: {
      Authorization: getAccessToken('accessToken'),
      'Content-Type': 'application/json',
    },
    url: `http://localhost:3030/users/${userID}`,
  });
};
