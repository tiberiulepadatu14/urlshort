import { SyntheticEvent } from "react";

export interface IUser {
    userName: string,
    password: string
}

export interface IUserRegister extends IUser {
    password2: string,
}

export interface IForm {
    formFunction: (e: SyntheticEvent, state: unknown) => void,
    formContent: Array<IFormFields>
    error: string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    setState: React.Dispatch<React.SetStateAction<any>>,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    state: any;
    buttonString: string,
    back?: boolean,
    formStyle?: unknown,
}

export interface IFormFields {
    label: string,
    name: string,
    type: string
}

export interface IURL {
    _id?: string, // id of the entry
    id?: string, // userID
    URLName: string,
    URLShort: string,
    URLHits: number,
}


export interface IURLParams {
    pageId: number;
}


export enum Permissions {
    USER = 'user',
}
export interface IUserData {
    _id: string,
    permissions: Array<Permissions>,
    createdAt: Date,
    updatedAt: Date,
    email: string
}
