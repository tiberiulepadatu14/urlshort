import React from 'react';
import { LoginContainer } from './containers/LoginContainer';
import {
  BrowserRouter as Router,
  Redirect,
  Switch,
  Route,
} from 'react-router-dom';
import PrivateRoute from './utils/PrivateRoute';
import ProtectedRoute from './utils/ProtectedRoute';
import { URLContiner } from './containers';
import { RegisterContainer } from './containers/RegisterContainer';
import { Home } from './containers/Home';

const App : React.FC = () => {
  return (
    <Router>
      <Switch>  
        <Route exact={true} path="/home" component={Home} />
        <ProtectedRoute exact={true} path="/login" component={LoginContainer} />
        <ProtectedRoute
          exact={true}
          path="/register"
          component={RegisterContainer}
        />
        <PrivateRoute component={URLContiner} {...{ path: '/:pageId' }} />
        <Route path="/">
          <Redirect to="/0" />
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
