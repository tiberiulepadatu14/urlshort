import { Button, Typography } from '@material-ui/core';
import React from 'react';
import { deleteUser } from '../../utils/api';
import { IUserData } from '../../utils/interface';

export const User = ({
  props,
  setUpdateComponent,
  handleOpen,
  setUpdateID,
}: {
  setUpdateID: React.Dispatch<React.SetStateAction<IUserData | undefined>>;
  props: IUserData;
  setUpdateComponent: React.Dispatch<React.SetStateAction<boolean>>;
  handleOpen: () => void;
}) => {
  const typographyValues = [
    {
      textValue: 'Name',
      textVariable: props.email,
    },
    {
      textValue: 'ID',
      textVariable: props._id,
    },
    {
      textValue: `Update Date`,
      textVariable: new Date(props.updatedAt).toDateString(),
    },
    {
      textValue: 'Create Date',
      textVariable: new Date(props.createdAt).toDateString(),
    },
  ];
  return (
    <div className="cardClass card">
      {
        // @ts-ignore
        typographyValues.map((field, index) => (
          <div key={field.textValue}>
            <div
              className="gridTextContainer"
              style={{
                display: 'grid',
                gridTemplateColumns: 'minmax(50px, 1fr) 1fr',
                width: '100%',
              }}
              title={`${field.textValue}: ${field.textVariable}`}
            >
              <Typography
                variant={index === 0 ? 'h5' : 'body1'}
                style={{
                  textTransform: 'capitalize',
                }}
              >
                <b> {`${field.textValue}:`}</b>
              </Typography>
              <Typography
                variant={index === 0 ? 'h5' : 'body1'}
                align="right"
                style={{
                  textTransform: 'capitalize',
                }}
              >
                {field.textVariable}
              </Typography>
            </div>
          </div>
        ))}

      <div className="buttonContainer">
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            if (props) setUpdateID({ ...props });
            handleOpen();
          }}
        >
          Update
        </Button>
        <Button
          variant="contained"
          color="secondary"
          onClick={() => {
            deleteUser(props._id).then(() =>
              setUpdateComponent((update) => {
                return !update;
              })
            );
          }}
        >
          Delete
        </Button>
      </div>
    </div>
  );
};
