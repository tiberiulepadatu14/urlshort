import React, { CSSProperties, SyntheticEvent, useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import {
  Button,
  Typography,
  FormControlLabel,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
} from '@material-ui/core';
import { IForm, IFormFields } from '../../utils/interface';
import { useHistory } from 'react-router-dom';
import Checkbox from '@material-ui/core/Checkbox';

export const FormBuilder: React.FC<IForm> = ({
  state,
  formFunction,
  formContent,
  setState,
  error,
  buttonString,
  back,
  formStyle,
}: IForm) => {
  const history = useHistory();
  const [localState, setLocalState] = useState(state);

  // I get an warning in the console because the material ui is using the react-transition group wich is using
  // the findDomNode which is deprecated in React :(

  useEffect(() => {
    if (Object.keys(state).includes('email')) {
      setLocalState({
        ...state,
        userName: (state)?.email,
      });
    }
  }, [state]);

  const gmtValues = new Set<number>();
  Array(13)
    .fill(0)
    .map((_, index) => {
      gmtValues.add(-index);
      gmtValues.add(index);
      return null;
    });

  return (
    <form
      autoComplete="off"
      onSubmit={(event: SyntheticEvent) => {
        const stateLocal = { ...localState };
        const email = localState['userName'];

        if (Object.keys(stateLocal).includes('userName')) {
          delete stateLocal['email'];
        }

        setState({ ...stateLocal, email });
        formFunction(event, { ...stateLocal, email });
      }}
      style={formStyle as CSSProperties}
    >
      {localState &&
        // @ts-ignore
        formContent.map((field: IFormFields, index: number) => {
          switch (field.type) {
            case 'checkbox':
              return (
                <FormControlLabel
                  key={field.label}
                  control={
                    <Checkbox
                      color="primary"
                      checked={localState[field.name]?.includes('admin')}
                      onChange={() => {
                        if (localState[field.name]?.includes('admin'))
                          setLocalState({
                            ...localState,
                            permissions: ['user'],
                          });
                        else
                          setLocalState({
                            ...localState,
                            permissions: ['user', 'admin'],
                          });
                      }}
                    />
                  }
                  label={'Admin'}
                />
              );
            case 'select':
              return (
                <FormControl key={field.label}>
                  <InputLabel id="simple-select-label">
                    GMT in that zone
                  </InputLabel>
                  <Select
                    name={field.name}
                    onChange={(e) => {
                      const { value } = e.target;
                      setLocalState({ ...localState, [field.name]: value });
                    }}
                    value={localState[field.name]}
                    style={{
                      width: '100%',
                    }}
                    labelId="simple-select-label"
                    id="simple-select"
                  >
                    {Array.from(gmtValues)
                      .sort(function (a, b) {
                        return a - b;
                      })
                      .map((e) => (
                        <MenuItem key={e} value={e}>
                          {e + ' GMT'}
                        </MenuItem>
                      ))}
                  </Select>
                </FormControl>
              );
            default:
              return (
                <TextField
                  key={index}
                  value={localState[field.name] ?? ''}
                  id={`outlined-basic-${field.name}`}
                  label={field.label}
                  name={field.name}
                  variant="outlined"
                  type={field.type}
                  onChange={(e) => {
                    const { name, value } = e.target;
                    setLocalState({ ...localState, [name]: value });
                  }}
                />
              );
          }
        })}
      <div
        style={{
          display: 'grid',
          gridTemplateColumns: '1fr 1fr',
          gridGap: '10px',
        }}
      >
        <Button
          type="submit"
          variant="contained"
          color="primary"
          style={{
            gridColumn: `1 / ${back ? 2 : 3}`,
            marginBottom: '30px',
          }}
        >
          {' '}
          {buttonString}{' '}
        </Button>
        {back ? (
          <Button
            variant="contained"
            style={{ marginBottom: '30px' }}
            color="secondary"
            onClick={(e) => {
              e.preventDefault();
              history.goBack();
            }}
          >
            {' '}
            Cancel{' '}
          </Button>
        ) : null}
      </div>
      {error !== '' && (
        <Typography
          style={{
            color: 'red',
          }}
        >
          Error: {error}
        </Typography>
      )}
    </form>
  );
};
