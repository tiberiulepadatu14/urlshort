import React from 'react';
import { Button, Typography } from '@material-ui/core';
import { deleteURL } from '../../utils/api';
import { IURL } from '../../utils/interface';



export const URL = (
    {
        props,
        setUpdateComponent,
        setUpdateID,
        handleOpen
    }: {
        props: IURL,
        setUpdateComponent: React.Dispatch<React.SetStateAction<boolean>>,
        setUpdateID: React.Dispatch<React.SetStateAction<IURL | undefined>>,
        handleOpen: () => void
    }) => {

    return (
        <div className='cardClass card'>
            <Text {...{ text: props.URLName, type: 'URL', variant: 'h5' }} />
            <Text {...{ text: props.URLShort, type: 'URL Short:', variant: 'subtitle1' }} />
            <Text {...{ text: props.URLHits, type: 'Number of uses:', variant: 'subtitle1' }} />

            <div className='buttonContainer'>
                <Button variant='contained' color='primary'
                    onClick={() => {
                        setUpdateID(props);
                        handleOpen();
                    }}> Update </Button>
                <Button variant='contained' color='secondary' onClick={
                    () => {
                        deleteURL(props._id ?? '');
                        setUpdateComponent(update => { return !update });
                    }
                }> Delete </Button>
            </div>
        </div >
    )
}

const Text = (props: any) => (
    <div className='gridTextContainer' style={
        {
            display: 'grid',
            gridTemplateColumns: 'minmax(50px, 1fr) 1fr',
            width: '100%'
        }
    } title={`${props.type} ${props.text}`}>
        <Typography variant={props.variant} style={{
            overflow: 'visible'
        }}>
            <b>{props.type}</b>
        </Typography>
        <Typography align='right' variant={props.variant}>
            {props.text}
        </Typography>
    </div>
);