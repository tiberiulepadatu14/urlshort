import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { IUserRegister } from '../../utils/interface';
import { FormBuilder } from '../../components/FormBuilder'
import { useHistory } from 'react-router-dom';
import { register } from '../../utils/api';


const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        width: 'calc(25ch + 60px)',
        minHeight: '35vh',
        maxWidth: '70vw',
        margin: 'auto',
        marginTop: '5vh',
        padding: '30px',
        borderRadius: '20px',
        border: '1px solid #647691',
        '& > form': {
            '& > *': {
                width: 'calc(100% - 30px)',
                margin: theme.spacing(2),
            }
        },
        '& > a': {
            position: 'absolute',
            bottom: '30px',
            left: '40px'
        }
    },
    mobileWidth: {
        width: '70vw',
    },
}));

export const RegisterContainer = () => {
    const history = useHistory();

    const classes = useStyles();

    const [state, setState] = useState<IUserRegister>(
        {
            userName: '',
            password: '',
            password2: '',
        }
    );

    const [error, setError] = useState<string>('');

    const formContent = [
        {
            label: 'Username',
            name: 'userName',
            type: 'input'
        },
        {
            label: 'Password',
            name: 'password',
            type: 'password'
        },
        {
            label: 'Confirm Password',
            name: 'password2',
            type: 'password'
        }]

    const onRegister = async (e: any, state: any) => {
        e.preventDefault();
        if (state.userName === '') {
            setError('Empty user name');
            return;
        } else if (state.password === '') {
            setError('Empty password');
            return;
        } else if (state.password !== state.password2) {
            setError('Passwords do not match');
            return;
        }
        setError('');

        try {
            await register({ ...state });
            history.push('/login')
        }
        catch (error) {
            try {
                console.log(error)
                setError(error.response.data.message.replace('email', 'User name'));
            }
            catch (error) {
                setError('Database connection error');
            }
        }
    };

    return (
        <div className={`${classes.root}`}>
            <FormBuilder {...{
                formFunction: onRegister,
                formContent,
                error,
                setState,
                state,
                buttonString: 'Register',
                back: true
            }} />
        </div>
    );
}
