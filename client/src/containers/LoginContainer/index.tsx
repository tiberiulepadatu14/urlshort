import React, { SyntheticEvent, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from '@material-ui/core';
import { setAccessToken } from '../../utils/accessTokenUtils';
import { IUser } from '../../utils/interface';
import { FormBuilder } from '../../components/FormBuilder';
import { useHistory } from 'react-router-dom';
import { logIn } from '../../utils/api';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    width: 'calc(25ch + 60px)',
    maxWidth: '70vw',
    minHeight: '35vh',
    margin: 'auto',
    marginTop: '5vh',
    padding: '30px',
    borderRadius: '20px',
    border: '1px solid #647691',
    '& > form': {
      '& > *': {
        width: 'calc(100% - 30px)',
        margin: theme.spacing(2),
      },
    },
    '& > a': {
      position: 'absolute',
      bottom: '30px',
      left: '40px',
    },
  },
  mobileWidth: {
    width: '70vw',
  },
}));

export const LoginContainer: React.FC = () => {
  const history = useHistory();
  const classes = useStyles();
  const [state, setState] = useState<IUser>({
    userName: '',
    password: '',
  });

  const [error, setError] = useState<string>('');

  const formContent = [
    {
      label: 'Username',
      name: 'userName',
      type: 'input',
    },
    {
      label: 'Password',
      name: 'password',
      type: 'password',
    },
  ];

  const onLogin = async (e: SyntheticEvent, state: unknown) => {
    e.preventDefault();
    if ((state as IUser).userName === '') {
      setError('Empty user name');
      return;
    } else if ((state as IUser).password === '') {
      setError('Empty password');
      return;
    }

    setError('');

    try {
      const data = await logIn({ ...(state as IUser) });
      setAccessToken('accessToken', (data as any)?.data?.accessToken);
      history.push('/0');
    } catch (error) {
      try {
        setError(error.response.data.message.replace('email', 'user name'));
      } catch (error) {
        setError('Database connection error');
      }
    }
  };

  return (
    <div className={`${classes.root}`}>
      <FormBuilder
        {...{
          formFunction: onLogin,
          formContent,
          error,
          setState,
          state,
          buttonString: 'Log In',
        }}
      />
      <Link href="/register">Register?</Link>
    </div>
  );
};
