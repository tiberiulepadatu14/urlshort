import { Button, Input, InputLabel, Link, makeStyles } from '@material-ui/core'
import axios from 'axios';
import React, { useState } from 'react'
import { useHistory } from 'react-router';

const SRV_ROOT = "http://localhost:3030/";

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        width: 'calc(25ch + 60px)',
        maxWidth: '70vw',
        minHeight: '35vh',
        margin: 'auto',
        marginTop: '5vh',
        padding: '30px',
        borderRadius: '20px',
        border: '1px solid #647691',
        '& > form': {
            '& > *': {
                width: 'calc(100% - 30px)',
                margin: theme.spacing(2),
            },
        },
        '& > a': {
            position: 'absolute',
            bottom: '30px',
            left: '40px',
        },
    },
    mobileWidth: {
        width: '70vw',
    },
}));

export const Home = () => {
    const [shortUrl, setShortUrl] = useState("");
    const [inputVal, setInputVal] = useState("");
    const history = useHistory();

    const classes = useStyles();
    return (
        <div style={{ display: "grid", placeItems: "center", marginTop: 20, }}>
            <div style={{
                width: 350,
            }}>
                <Button onClick={() => history.push("/login")}
                    variant="contained"
                    color="primary">
                    Go to login
                </Button>
            </div>
            <div className={`${classes.root}`}>
                <form style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "space-between",
                }}
                    onSubmit={(e) => {
                        e.preventDefault();
                        axios.post(`${SRV_ROOT}crud-url`, {
                            url: inputVal,
                        }).then(ctx => setShortUrl(`${SRV_ROOT}${ctx.data.shortUrl}`)).catch(e => console.error(e));
                    }}
                >
                    <>
                        <InputLabel htmlFor="my-input">URL to shorten</InputLabel>
                        <Input id="my-input" aria-describedby="url-short" onChange={(e) => setInputVal(e.target.value)} value={inputVal} />
                        <InputLabel style={{
                            marginTop: "30px",
                        }}>
                            Short URL: <Link href={shortUrl}>{shortUrl}</Link>
                        </InputLabel>
                    </>
                    <Button type="submit"
                        variant="contained"
                        color="primary" >
                        Submit
                    </Button>

                </form>
            </div>
        </div>
    )
}

