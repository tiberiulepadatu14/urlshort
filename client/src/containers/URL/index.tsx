import React, { useEffect, useState } from 'react';
import AddIcon from '@material-ui/icons/Add';
import { Button, Fab, Input, InputLabel, Link } from '@material-ui/core';
import {
  readUrls,
  eraseAccessToken,
  createUrl,
} from '../../utils/api';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import {
  IURL,
  IURLParams,
} from '../../utils/interface';
import { URL } from '../../components/URL';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import LastPageIcon from '@material-ui/icons/LastPage';
import { FirstPage, NavigateBefore } from '@material-ui/icons';
import { RouteComponentProps, useHistory, withRouter } from 'react-router-dom';
import { useMediaQuery } from 'react-responsive';
import axios from 'axios';


const SRV_ROOT = "http://localhost:3030/";

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    width: 'calc(25ch + 60px)',
    maxWidth: '70vw',
    minHeight: '35vh',
    margin: 'auto',
    marginTop: '5vh',
    padding: '30px',
    borderRadius: '20px',
    border: '1px solid #647691',
    '& > form': {
      '& > *': {
        width: 'calc(100% - 30px)',
        margin: theme.spacing(2),
      },
    },
    '& > a': {
      position: 'absolute',
      bottom: '30px',
      left: '40px',
    },
  },
  mobileWidth: {
    width: '70vw',
  },
  paper: {
    outline: 'none',
    position: 'absolute',
    height: '60vh',
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const _URL = (props: RouteComponentProps) => {
  const mobile = useMediaQuery({
    query: '(max-device-width: 1224px)',
  });
  const history = useHistory();
  const classes = useStyles();
  const [modalStyle] = useState(getModalStyle);
  const [open, setOpen] = useState(false);
  const [urls, setUrls] = useState<Array<IURL>>();
  const [updateComponent, setUpdateComponent] = useState(true);
  const [updateID, setUpdateID] = useState<IURL>();
  const [disable, setDisable] = useState({
    next: true,
    last: true,
    prev: true,
    first: true,
  });
  const [shortUrl, setShortUrl] = useState("");
  const [inputVal, setInputVal] = useState("");

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setUpdateID(undefined);
    setOpen(false);
  };

  const handleFirstPage = () => {
    history.push(`/0`);
  };

  const handleBefore = () => {
    history.push(`/${(props.match.params as IURLParams)?.pageId - 1}`);
  };

  const handleNext = () => {
    history.push(
      `/${Number((props.match.params as IURLParams)?.pageId as number) + 1
      }`
    );
  };

  const handleLast = () => {
    history.push(`/${Number.MAX_SAFE_INTEGER}`);
  };



  const urlCRUD = (
    <div style={modalStyle} className={`${classes.paper} widthClassPaper`}>
      <h2 id="simple-modal-title">
        {updateID ? 'Change the url to a new one' : 'Add a new url'}
      </h2>
      <div className={`${classes.root}`}>
        <form style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
        }}
          onSubmit={(e) => {
            console.log("here");
            e.preventDefault();
            createUrl({
              url: inputVal
            });
          }}
        >
          <>
            <InputLabel htmlFor="my-input">URL to shorten</InputLabel>
            <Input id="my-input" aria-describedby="url-short" onChange={(e) => setInputVal(e.target.value)} value={inputVal} />
            <InputLabel style={{
              marginTop: "30px",
            }}>
              Short URL: <Link href={shortUrl}>{shortUrl}</Link>
            </InputLabel>
          </>
          <Button type="submit"
            variant="contained"
            color="primary" >
            Submit
          </Button>

        </form>
      </div>
    </div>
  );

  useEffect(() => {
    readUrls(
      (props.match.params as IURLParams)?.pageId ?? 0,
      mobile ? 3 : 6
    )
      .then(({ urls, page, lastPage }) => {
        setDisable({
          last: lastPage ?? false,
          next: lastPage ?? false,
          first: -(page ?? 0) === 0,
          prev: -(page ?? 0) === 0,
        });

        if (page !== (props.match.params as IURLParams)?.pageId) {
          history.push(`/${page}`);
        }
        setUrls(urls?.data?.data);
      })
      .catch(
        (error) =>
          error.response.data.message === 'jwt expired' &&
          eraseAccessToken('accessToken') &&
          history.push('/login')
      );
  }, [open, updateComponent, props.match.params, history, mobile]);

  return (
    <>
      <Modal
        style={{
          margin: "auto",
          width: "40vw",
        }}
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {urlCRUD}
      </Modal>
      <div style={{
        margin: 20
      }}>
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            eraseAccessToken('accessToken');
            history.push('/login');
          }}
        >
          {' '}
          Log out
        </Button>
      </div>
      <div style={{
        width: "100vw",
        display: "grid",
        placeItems: "center",
      }}>
        {urls &&
          // @ts-ignore
          urls.map(urlInfo => (
            <URL
              setUpdateComponent={setUpdateComponent}
              setUpdateID={setUpdateID}
              handleOpen={handleOpen}
              props={urlInfo}
              key={urlInfo._id}
            />
          ))}
        <div style={{
          width: "100vw"
        }}>
          <Fab
            style={{
              marginLeft: 50,
            }}
            color="primary"
            aria-label="add"
            onClick={handleOpen}
            size="small"
          >
            <AddIcon />
          </Fab>
        </div>
        <div>
          <Fab
            color="primary"
            size="small"
            aria-label="firstPage"
            onClick={handleFirstPage}
            disabled={disable.first}
          >
            <FirstPage />
          </Fab>
          <Fab
            color="primary"
            size="small"
            aria-label="beforePage"
            onClick={handleBefore}
            disabled={disable.prev}
          >
            <NavigateBefore />
          </Fab>
          <Fab
            color="primary"
            size="small"
            aria-label="nextPage"
            onClick={handleNext}
            disabled={disable.next}
          >
            <NavigateNextIcon />
          </Fab>
          <Fab
            color="primary"
            size="small"
            aria-label="lastPage"
            onClick={handleLast}
            disabled={disable.last}
          >
            <LastPageIcon />
          </Fab>
        </div>
      </div>
    </>
  );
};

function getModalStyle() {
  return {
    margin: 'auto',
    paddingBottom: '50px',
    left: '0',
    right: '0',
    bottom: '0',
    top: '0',
  };
}


export const URLContiner = withRouter(_URL);
