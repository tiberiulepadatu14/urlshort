"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = __importDefault(require("../../src/app"));
describe('\'crud-url\' service', () => {
    it('registered the service', () => {
        const service = app_1.default.service('crud-url');
        expect(service).toBeTruthy();
    });
});
