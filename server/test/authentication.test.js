"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = __importDefault(require("../src/app"));
describe('authentication', () => {
    it('registered the authentication service', () => {
        expect(app_1.default.service('authentication')).toBeTruthy();
    });
    describe('local strategy', () => {
        const userInfo = {
            email: 'someone@example.com',
            password: 'supersecret'
        };
        beforeAll(async () => {
            try {
                await app_1.default.service('users').create(userInfo);
            }
            catch (error) {
                // Do nothing, it just means the user already exists and can be tested
            }
        });
        it('authenticates user and creates accessToken', async () => {
            const { user, accessToken } = await app_1.default.service('authentication').create({
                strategy: 'local',
                ...userInfo
            }, {});
            expect(accessToken).toBeTruthy();
            expect(user).toBeTruthy();
        });
    });
});
