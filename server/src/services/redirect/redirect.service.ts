// Initializes the `redirect` service on path `/`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Redirect } from './redirect.class';
import createModel from '../../models/redirect.model';
import app from '../../app';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    '': Redirect & ServiceAddons<any>;
  }
}

async function redirect(req: any, res: { redirect: (arg0: number, arg1: string) => any; }) {
  const data = await app.service('crud-url').find(
    {
      query: {
        shortUrl: req.params.id
      }
    });
  return res.redirect(301, (data as any).data[0].url);
}


export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/:id', new Redirect(options, app), redirect);

}
