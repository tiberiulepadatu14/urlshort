"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const redirect_class_1 = require("./redirect.class");
const redirect_model_1 = __importDefault(require("../../models/redirect.model"));
const app_1 = __importDefault(require("../../app"));
async function redirect(req, res) {
    const data = await app_1.default.service('crud-url').find({
        query: {
            shortUrl: req.params.id
        }
    });
    return res.redirect(301, data.data[0].url);
}
function default_1(app) {
    const options = {
        Model: redirect_model_1.default(app),
        paginate: app.get('paginate')
    };
    // Initialize our service with any options it requires
    app.use('/:id', new redirect_class_1.Redirect(options, app), redirect);
}
exports.default = default_1;
