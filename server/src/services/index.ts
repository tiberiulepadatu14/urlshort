import { Application } from '../declarations';
import users from './users/users.service';
import crudUrl from './crud-url/crud-url.service';
import redirect from './redirect/redirect.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application): void {
  app.configure(users);
  app.configure(crudUrl);
  app.configure(redirect);
}
