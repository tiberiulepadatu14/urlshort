// Initializes the `crud-url` service on path `/crud-url`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { CrudUrl } from './crud-url.class';
import createModel from '../../models/crud-url.model';
import hooks from './crud-url.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'crud-url': CrudUrl & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/crud-url', new CrudUrl(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('crud-url');

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  service.hooks(hooks);
}
