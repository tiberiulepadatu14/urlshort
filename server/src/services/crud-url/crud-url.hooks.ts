import { setField } from 'feathers-authentication-hooks';
import decode from 'jwt-decode';


export default {
  before: {
    all: [],
    find: [],
    get: [async (context: any) => {
      const userId = (decode(context.params.headers.authorization) as any)?.sub ?? 'undefined';
      console.log(userId);
      if (userId !== 'undefined') {
        context.params.userID = userId;
        console.log(userId);
      }
    }, setField({
      from: 'params.userId',
      as: 'params.query.userId'
    })],
    create: [async (context: any) => {
      let userId;
      console.log("aaa");
      try {
        userId = (decode(context.params.headers.authorization) as any)?.sub ?? 'undefined';
      } catch {
        userId = 'undefined';
      }
      console.log({ userId });
      while (true) {
        const newUrl = Math.random().toString(36).substring(5);
        const data = await context.service.find({
          query: {
            shortUrl: newUrl
          }
        });
        console.log(data);
        console.log({ data });
        if (data.total === 0) {
          context.data.shortUrl = newUrl;
          context.data.userId = userId;
          return context;
        }
      }
    }],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};


