"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const users_service_1 = __importDefault(require("./users/users.service"));
const crud_url_service_1 = __importDefault(require("./crud-url/crud-url.service"));
const redirect_service_1 = __importDefault(require("./redirect/redirect.service"));
// Don't remove this comment. It's needed to format import lines nicely.
function default_1(app) {
    app.configure(users_service_1.default);
    app.configure(crud_url_service_1.default);
    app.configure(redirect_service_1.default);
}
exports.default = default_1;
